package com.rockycamacho.remoteoutlet;

import android.app.Application;
import android.content.Context;

import com.frogermcs.androiddevmetrics.AndroidDevMetrics;
import com.rockycamacho.remoteoutlet.injections.AppComponent;
import com.rockycamacho.remoteoutlet.injections.AppModule;
import com.rockycamacho.remoteoutlet.injections.DaggerAppComponent;
import com.rockycamacho.remoteoutlet.views.activities.RemoteOutletsActivity;

import timber.log.Timber;

/**
 * Created by Rocky Camacho on 3/7/2016.
 */
public class RemoteOutletApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            AndroidDevMetrics.initWith(this);
        }
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static RemoteOutletApplication get(Context context) {
        return (RemoteOutletApplication) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
