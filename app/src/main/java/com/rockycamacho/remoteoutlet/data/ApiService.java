package com.rockycamacho.remoteoutlet.data;

import com.rockycamacho.remoteoutlet.data.models.GetStatusResponse;
import com.rockycamacho.remoteoutlet.data.models.RemoteOutlet;

import java.util.ArrayList;

import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;
import rx.Single;

/**
 * Created by Rocky Camacho on 3/8/2016.
 */
public interface ApiService {

    @GET
    Observable<GetStatusResponse> getStatus(@Url String url);

    @GET
    Observable<GetStatusResponse> turnOn(@Url String url);

    @GET
    Observable<GetStatusResponse> turnOff(@Url String url);

}
