package com.rockycamacho.remoteoutlet.data;

import com.rockycamacho.remoteoutlet.data.models.GetStatusResponse;
import com.rockycamacho.remoteoutlet.data.models.RemoteOutlet;

import java.util.ArrayList;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Rocky Camacho on 3/7/2016.
 */
public class RemoteOutletRepository {

//    public static final String TEST_URL = "http://192.168.56.1:9090/";
    public static final String TEST_URL = "http://192.168.1.13/";
    private final ApiService apiService;

    public RemoteOutletRepository(ApiService apiService) {
        this.apiService = apiService;
    }

    public Observable<ArrayList<RemoteOutlet>> fetchOutlets() {
        return apiService.getStatus(TEST_URL + "status")    //TODO: change url
                .map(new Func1<GetStatusResponse, ArrayList<RemoteOutlet>>() {
                    @Override
                    public ArrayList<RemoteOutlet> call(GetStatusResponse response) {
                        return response.getItems();
                    }
                });
    }

    public Observable<ArrayList<RemoteOutlet>> turnOn(RemoteOutlet item) {
        return apiService.turnOn(TEST_URL + item.getOrder() + "/on")    //TODO: change url
                .map(new Func1<GetStatusResponse, ArrayList<RemoteOutlet>>() {
                    @Override
                    public ArrayList<RemoteOutlet> call(GetStatusResponse response) {
                        return response.getItems();
                    }
                });
    }

    public Observable<ArrayList<RemoteOutlet>> turnOff(RemoteOutlet item) {
        return apiService.turnOff(TEST_URL + item.getOrder() + "/off")    //TODO: change url
                .map(new Func1<GetStatusResponse, ArrayList<RemoteOutlet>>() {
                    @Override
                    public ArrayList<RemoteOutlet> call(GetStatusResponse response) {
                        return response.getItems();
                    }
                });
    }
}
