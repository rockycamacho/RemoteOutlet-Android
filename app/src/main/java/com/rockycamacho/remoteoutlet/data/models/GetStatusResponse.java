package com.rockycamacho.remoteoutlet.data.models;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.ArrayList;

/**
 * Created by Rocky Camacho on 3/8/2016.
 */
@JsonObject
public class GetStatusResponse {

    @JsonField
    private ArrayList<RemoteOutlet> items;

    public ArrayList<RemoteOutlet> getItems() {
        return items;
    }

    public void setItems(ArrayList<RemoteOutlet> items) {
        this.items = items;
    }
}
