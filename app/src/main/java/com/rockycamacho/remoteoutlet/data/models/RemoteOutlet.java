package com.rockycamacho.remoteoutlet.data.models;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by Rocky Camacho on 3/7/2016.
 */
@JsonObject
public class RemoteOutlet {

    @JsonField
    private String name;
    @JsonField
    private int status;
    @JsonField
    private int order;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
