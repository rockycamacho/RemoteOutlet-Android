package com.rockycamacho.remoteoutlet.injections;

import com.rockycamacho.remoteoutlet.views.activities.RemoteOutletsActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Rocky Camacho on 3/8/2016.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(RemoteOutletsActivity activity);

}
