package com.rockycamacho.remoteoutlet.injections;

import android.app.Application;

import com.github.aurae.retrofit2.LoganSquareConverterFactory;
import com.rockycamacho.remoteoutlet.BuildConfig;
import com.rockycamacho.remoteoutlet.data.ApiService;
import com.rockycamacho.remoteoutlet.data.RemoteOutletRepository;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Scheduler;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Rocky Camacho on 3/8/2016.
 */
@Module
public class AppModule {

    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Singleton
    @Provides
    ApiService providesApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(CallAdapter.Factory callAdapterFactory, Converter.Factory converterFactory, OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl("http://www.jsontest.com/")
                .addCallAdapterFactory(callAdapterFactory)
                .addConverterFactory(converterFactory)
                .client(client)
                .build();
    }

    @Singleton
    @Provides
    CallAdapter.Factory providesCallAdapterFactory() {
        return RxJavaCallAdapterFactory.create();
    }

    @Singleton
    @Provides
    Converter.Factory providesConverterFactory() {
        return LoganSquareConverterFactory.create();
    }

    @Singleton
    @Provides
    OkHttpClient providesOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if(BuildConfig.DEBUG) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                @Override
                public void log(String message) {
                    Timber.tag("OKHTTP").d(message);
                }
            });
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(httpLoggingInterceptor);
        }
        return builder.build();
    }

    @Singleton
    @Provides
    Scheduler provideScheduler() {
        return Schedulers.io();
    }

    @Provides
    RemoteOutletRepository provideRemoteOutletRepository(ApiService apiService) {
        return new RemoteOutletRepository(apiService);
    }

}
