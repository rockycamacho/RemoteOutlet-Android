package com.rockycamacho.remoteoutlet.presenters;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.rockycamacho.remoteoutlet.data.RemoteOutletRepository;
import com.rockycamacho.remoteoutlet.data.models.RemoteOutlet;
import com.rockycamacho.remoteoutlet.views.RemoteView;
import com.rockycamacho.remoteoutlet.views.activities.RemoteOutletsActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Observer;
import rx.Scheduler;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

/**
 * Created by Rocky Camacho on 3/7/2016.
 */
public class RemotePresenter extends MvpBasePresenter<RemoteView> {

    private final RemoteOutletRepository repository;
    private final Scheduler scheduler;
    private Subscription subscription;

    @Inject
    public RemotePresenter(RemoteOutletRepository repository, Scheduler scheduler) {
        this.repository = repository;
        this.scheduler = scheduler;
    }

    @Override
    public void attachView(RemoteView view) {
        super.attachView(view);
        //TODO: attach running operations
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        //TODO: unsubscribe running operations
    }

    public void fetchOutlets() {
        if(isViewAttached()) {
            getView().showLoading();
            subscription = repository.fetchOutlets()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(scheduler)
                    .subscribe(new Observer<ArrayList<RemoteOutlet>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            Timber.e(e, e.getMessage());
                        }

                        @Override
                        public void onNext(ArrayList<RemoteOutlet> remoteOutlets) {
                            if (isViewAttached()) {
                                getView().loadData(remoteOutlets);
                            }
                        }
                    });
        }
    }

    public void turnOn(RemoteOutlet item) {
        repository.turnOn(item)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(scheduler)
                .subscribe(new Observer<ArrayList<RemoteOutlet>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, e.getMessage());
                    }

                    @Override
                    public void onNext(ArrayList<RemoteOutlet> remoteOutlets) {
                        if (isViewAttached()) {
                            getView().loadData(remoteOutlets);
                        }
                    }
                });
    }

    public void turnOff(RemoteOutlet item) {
        repository.turnOff(item)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(scheduler)
                .subscribe(new Observer<ArrayList<RemoteOutlet>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, e.getMessage());
                    }

                    @Override
                    public void onNext(ArrayList<RemoteOutlet> remoteOutlets) {
                        if (isViewAttached()) {
                            getView().loadData(remoteOutlets);
                        }
                    }
                });
    }
}
