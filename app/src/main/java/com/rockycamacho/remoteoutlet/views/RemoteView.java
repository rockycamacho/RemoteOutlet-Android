package com.rockycamacho.remoteoutlet.views;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.rockycamacho.remoteoutlet.data.models.RemoteOutlet;

import java.util.ArrayList;

/**
 * Created by Rocky Camacho on 3/7/2016.
 */
public interface RemoteView extends MvpView {

    void showLoading();
    void showContent();
    void showError();
    void loadData(ArrayList<RemoteOutlet> items);
}
