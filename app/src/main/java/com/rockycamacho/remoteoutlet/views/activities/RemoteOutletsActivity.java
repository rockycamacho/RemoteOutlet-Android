package com.rockycamacho.remoteoutlet.views.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.rockycamacho.remoteoutlet.R;
import com.rockycamacho.remoteoutlet.RemoteOutletApplication;
import com.rockycamacho.remoteoutlet.data.models.RemoteOutlet;
import com.rockycamacho.remoteoutlet.presenters.RemotePresenter;
import com.rockycamacho.remoteoutlet.views.RemoteView;
import com.rockycamacho.remoteoutlet.views.adapters.OnItemClickListener;
import com.rockycamacho.remoteoutlet.views.adapters.RemoteOutletsAdapter;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Provider;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class RemoteOutletsActivity extends MvpActivity<RemoteView, RemotePresenter> implements RemoteView {

    @Inject
    Provider<RemotePresenter> remotePresenterProvider;

    @Bind(R.id.list)
    RecyclerView list;

    private RemoteOutletsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        RemoteOutletApplication.get(this).getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote);
        ButterKnife.bind(this);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setHasFixedSize(true);
        adapter = new RemoteOutletsAdapter();
        adapter.setOnTurnOnCallback(new OnItemClickListener<RemoteOutlet>() {
            @Override
            public void onItemClick(RemoteOutlet item, int position) {
                turnOn(item);
            }
        });
        adapter.setOnTurnOffCallback(new OnItemClickListener<RemoteOutlet>() {
            @Override
            public void onItemClick(RemoteOutlet item, int position) {
                turnOff(item);
            }
        });
        list.setAdapter(adapter);
    }

    private void turnOn(RemoteOutlet item) {
        presenter.turnOn(item);
    }

    private void turnOff(RemoteOutlet item) {
        presenter.turnOff(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.fetchOutlets();
    }

    @NonNull
    @Override
    public RemotePresenter createPresenter() {
        return remotePresenterProvider.get();
    }

    @Override
    public void showLoading() {
        Timber.d("Show Loading");
    }

    @Override
    public void showContent() {
        Timber.d("Show Content");
    }

    @Override
    public void showError() {
        Timber.d("Show Error");
    }

    @Override
    public void loadData(ArrayList<RemoteOutlet> items) {
        adapter.setItems(items);
        showContent();
    }
}
