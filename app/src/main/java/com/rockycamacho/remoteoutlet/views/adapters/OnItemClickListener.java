package com.rockycamacho.remoteoutlet.views.adapters;

/**
 * Created by Rocky Camacho on 3/7/2016.
 */
public interface OnItemClickListener<T> {

    void onItemClick(T item, int position);

}
