package com.rockycamacho.remoteoutlet.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.rockycamacho.remoteoutlet.R;
import com.rockycamacho.remoteoutlet.data.models.RemoteOutlet;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Rocky Camacho on 3/7/2016.
 */
public class RemoteOutletsAdapter extends RecyclerView.Adapter<RemoteOutletsAdapter.ViewHolder> {

    private final static int LAYOUT_RES_ID = R.layout.list_item_remote_outlet;

    private ArrayList<RemoteOutlet> items;
    private OnItemClickListener<RemoteOutlet> onTurnOnCallback;
    private OnItemClickListener<RemoteOutlet> onTurnOffCallback;

    public RemoteOutletsAdapter() {
        this.items = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(LAYOUT_RES_ID, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final RemoteOutlet item = items.get(position);
        holder.name.setText(item.getName());
        holder.turnOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onTurnOnCallback != null) {
                    onTurnOnCallback.onItemClick(item, position);
                }
            }
        });
        holder.turnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onTurnOffCallback != null) {
                    onTurnOffCallback.onItemClick(item, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(ArrayList<RemoteOutlet> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setOnTurnOnCallback(OnItemClickListener<RemoteOutlet> onTurnOnCallback) {
        this.onTurnOnCallback = onTurnOnCallback;
    }

    public void setOnTurnOffCallback(OnItemClickListener<RemoteOutlet> onTurnOffCallback) {
        this.onTurnOffCallback = onTurnOffCallback;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.turn_off)
        Button turnOff;
        @Bind(R.id.turn_on)
        Button turnOn;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
