package com.rockycamacho.remoteoutlet;

import com.github.aurae.retrofit2.LoganSquareConverterFactory;
import com.rockycamacho.remoteoutlet.data.ApiService;
import com.rockycamacho.remoteoutlet.data.models.GetStatusResponse;
import com.rockycamacho.remoteoutlet.data.models.RemoteOutlet;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import timber.log.Timber;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by Rocky Camacho on 3/8/2016.
 */
public class ApiServiceTest {

    private ApiService apiService;

    @Before
    public void setUp() throws Exception {
        Timber.plant(new Timber.Tree() {
            @Override
            protected void log(int priority, String tag, String message, Throwable t) {
                if (t != null) {
                    System.err.println(t.getMessage());
                    t.printStackTrace();
                } else {
                    System.out.println(message);
                }
            }
        });

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.tag("OKHTTP").d(message);
            }
        });
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://localhost:9090/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(LoganSquareConverterFactory.create())
                .client(okHttpClient)
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    @Test
    public void testGetStatus() throws Exception {
        GetStatusResponse getStatusResponse = apiService.getStatus("http://localhost:9090/" + "status")
                .toBlocking()
                .first();
        ArrayList<RemoteOutlet> data = getStatusResponse.getItems();
        assertNotNull(data);
        assertEquals(1, data.size());
    }
}
